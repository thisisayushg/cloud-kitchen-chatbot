## Problem Statement

An Indian startup named 'Foodie' wants to build a conversational bot (chatbot) which can help users discover restaurants across several Indian cities. You have been hired as the lead data scientist for creating this product.
 

The main purpose of the bot is to help users discover restaurants quickly and efficiently and to provide a good restaurant discovery experience. The project brief provided to you is as follows.
 

The bot takes the following inputs from the user:

*City*: Take the input from the customer as a text field. For example:

> **Bot**: In which city are you looking for restaurants?

> **User**: anywhere in Delhi

<br/>
<br/>

*Important Notes:*

Assume that Foodie works only in Tier-1 and Tier-2 cities. You can use the current HRA classification of the cities from here. Under the section 'current classification' on this page, the table categorizes cities as X, Y and Z. Consider 'X ' cities as tier-1 and 'Y' as tier-2. 
The bot should be able to identify common synonyms of city names, such as Bangalore/Bengaluru, Mumbai/Bombay etc.
 

Your chatbot should provide results for tier-1 and tier-2 cities only, while for tier-3 cities, it should reply back with something like "We do not operate in that area yet".

<br/>
 

*Cuisine Preference:* Take the cuisine preference from the customer. The bot should list out the following six cuisine categories (Chinese, Mexican, Italian, American, South Indian & North Indian) and the customer can select any one out of that. Following is an example for the same:

> **Bot:** What kind of cuisine would you prefer?
> - Chinese
> - Mexican
> - Italian
> - American
> - South Indian
> - North Indian
>
> **User**: I’ll prefer Italian!

<br/>

 
*Average budget for two peopl*e: Segment the price range (average budget for two people) into three price categories: lesser than 300, 300 to 700 and more than 700. The bot should ask the user to select one of the three price categories. For example:

> **Bot**: What price range are you looking at?
> 
> - Lesser than Rs. 300
> - Rs. 300 to 700
> - More than 700
>
> **User**: in range of 300 to 700
 
<br/>

While showing the results to the user, the bot should display the top 5 restaurants in a sorted order (descending) of the average Zomato user rating (on a scale of 1-5, 5 being the highest). The format should be: {restaurant_name} in {restaurant_address} has been rated {rating}.


Finally, the bot should ask the user whether he/she wants the details of the top 10 restaurants on email. If the user replies 'yes', the bot should ask for user’s email id and then send it over email. Else, just reply with a 'goodbye' message. The mail should have the following details for each restaurant:

1. Restaurant Name
2. Restaurant locality address
3. Average budget for two people
4. Zomato user rating


You have been given some sample conversational stories in the ‘test’ file. Look at the stories and try to observe entities, intents, dialogue-flow which may be useful to train the NLU and also the dialogue flow.

<br/>
<br/>

## Dataset 
*The dataset is available in `zomato.csv` file*

<br/>

<br/>

## Rasa Chatbot Setup Instructions
<br/>


On Linux, open Terminal. On Windows, search in Start Menu for Anaconda Prompt.

Change the current directory to the project folder ('rasa_basic_folder' by default) and then 
please follow the below setup instructions for running the project:
Note: you need internet connection for this setup.

1. Check the list of all virtual environments in your system:
    
    `conda info --envs`

2. If you have an environment named 'rasa', remove it with following command:
    
    `conda remove -n rasa --all`

3. Create a new environment with 'rasa' name and python version 3.7.9:
    
    `conda create -n rasa python==3.7.9`

4. Activate the environment:
    
    `conda activate rasa`

5. Install the required packages for this chatbot:

    Method 1[Using requirements file]: <br/>
    `pip install -r requirements.txt`     	(For Linux)
    <br/>OR <br/>
    `pip install -r requirements_win.txt` 	(For Windows)

    <br/>
    Method 2[Recommended for both Linux and Windows]:<br/>

    `pip install rasa==2.6.0`<br/>
    `pip install rasa[spacy]`<br/>
    `python -m spacy download en_core_web_md`<br/>
    `pip install pandas`<br/>
    `pip install aiosmtplib`<br/>
    `pip install nltk`<br/>

    <br/>

    *NOTE:* If you get an error saying 'Cannot install `<packagename>`. It is a     distutils package and thus we cannot accurately 
    determine which files belong to it which would lead to only a partial install',     use -ignore-installed flag:<br/>
    `pip install -r requirements.txt --ignore-installed`

6. Start the chatbot:

    `rasa shell`