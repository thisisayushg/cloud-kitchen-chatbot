## intent:affirm
- yes
- yep
- yup
- yeah
- ya
- indeed
- that's right
- ok
- okay
- ohk
- it's ok
- great
- sure
- of course
- correct
- great choice
- sounds good
- sounds good to me
- sounds great
- right thanks
- yes thank you
- okay thank you
- yes thanks
- yup thanks
- ok thanks
- nice
- confirm
- accept
- cool
- okay cool
- alright
- okie dokie
- perfect
- sure thing
- absolutely
- amazing
- sounds perfect
- yay
- definitely
- fine
- agreed
- affirmative

## intent:goodbye
- bye
- tata
- see you
- see ya
- good bye
- bye tc
- bye gn
- bbye take care
- farewell
- Bye bye
- have a good day
- bye thank you
- yes thanks
- great thanks
- ok bye
- ciao
- asta lavista
- catch you later
- bye for now
- gotta go
- got to go
- good night
- bye then
- until next time
- go to hell

## intent:greet
- hey
- howdy
- heya
- hello
- hi
- yo
- hey there
- hi there
- good morning
- good evening
- dear
- hi, can you help me ?
- hi, will you help me ?
- greetings
- hola
- hola amigo
- hi, anyone there ?
- hi again
- hi, help me
- what's up!
- how are you doing
- how ya doing ?
- start

## intent:deny
- no
- not really
- nah
- not at all
- none
- nope
- absolutely not
- never
- incorrect
- cancel
- deny
- i deny
- decline
- no thanks
- nah thanks
- not right
- never mind
- nevermind
- this is not right
- i can't
- no i don't accept it
- neither
- none of the above
- none of these
- no it's wrong
- no its not right
- not really thanks
- no again
- no, not really
- i am not sure about it
- not sure
- really not sure
- no idea
- don't know
- dont give wrong suggestions
- dont utter nonsense
- no idea about it
- i don't want this
- doesn't seem right
- negative

## intent:thanks
- thanks
- thank you
- thanks to you
- thank u
- thanks a lot
- thanks alot
- thank you so much
- thank you so very much
- ok thanks
- ok thank you
- yes thanks
- yes thank you
- thanks dear
- thank you dear
- thanks for the help
- thanks for helping
- nice thanks
- awesome thanks
- amazing thanks
- perfect thanks
- perfect thank you
- sounds great thanks
- sounds great thank you
- thanks for booking the table in [mumbai](location)
- thank you for ordering [chinese](cuisine) for me
- thanks for the restaurant advice in [Mirzapur](location)

## intent:restaurant_search
- i'm looking for a place to eat
- I want to grab [lunch](mealtype)
- I am searching for a [dinner](mealtype) spot
- I am looking for some restaurants in [New Delhi](location).
- I am looking for some restaurants in [Bangalore](location)
- show me [chinese](cuisine) restaurants
- show me a [mexican](cuisine) place in the [centre](location)
- i am looking for an [indian](cuisine) spot called olaolaolaolaolaola
- search for restaurants
- anywhere in the [west](location)
- I am looking for [asian fusion](cuisine) food
- I am looking a restaurant in [Puducherry](location)
- in [Gurgaon](location)
- [south indian](cuisine)
- [north indian](cuisine)
- [Lithuania](location)
- Oh, sorry, in [Italy](location)
- in [new delhi](location)
- I am looking for some restaurants in [Mumbai](location)
- I am looking for [mexican indian fusion](cuisine)
- can you book a table in [rome](location) in a [moderate](budget:mid) price range with [burger](cuisine) food for [4](people) people
- [central](location) [indian](cuisine) restaurant
- please help me to find restaurants in [pune](location)
- Please find me a restaurant in [bangalore](location)
- [mumbai](location)
- show me restaurants
- please find me [chinese](cuisine) restaurant in [new delhi](location)
- can you find me a [chinese](cuisine) restaurant in [Amritsar](location) for [300](price_lowerbound) to [400](price_upperbound) for a meal?
- can you find me a [chinese](cuisine) restaurant in [Amritsar](location) for less than [300](price_upperbound) for a meal?
- please find me a restaurant in [ahmedabad](location)
- please show me a few [italian](cuisine) restaurants in [bangalore](location)
- [mexican](cuisine) with friends
- Date Night in [bangalore](location)
- I want to grab [italian](cuisine) today
- i want to eat [masala dosa](cuisine:south indian)
- What do you have in [american](cuisine) tomorrow?
- Is there any place with [North Indian](cuisine) in [bangalore](location)
- Where can I find [pizza](cuisine)
- What are the good places in [bangalore](location) for [pizza](cuisine)
- which dining places have [best](rating:4.5) [paneer do pyaza](cuisine:north indian)?
- [Ranchi](location) restaurants with good [pasta](cuisine)
- Find me some veg restaurants in [Nasik](location)
- I am looking for a good place to grab [lunch](mealtype)
- find something [north indian](cuisine) to eat but in [budget](budget:low)
- this is [expensive](budget:high). find something else.
- suggest something for [breakfast](mealtype)
- suggest a good place for [lunch](mealtype)
- Order [chicken](cuisine:american) for my [dinner](mealtype)
- I love to have [french fries](cuisine) with [coke](beverage)
- Where can I find a good [cold coffee](beverage)
- [Pizza](cuisine:italian) without [coke](beverage) is waste
- group of [8](people)
- group of [5](people)
- we are [11](people) persons
- [hotel nawab](location) is a [5 star](rating:5) restaurant
- We would like to have [Idli Sambhar](cuisine) in complimentary [breakfast](mealtype)
- please tell me dining places in [low](budget) budget
- [luxurious](budget:high) restaurants with [south indian](cuisine) dishes
- Hangout places with [mexican](cuisine) servings
- find me a restaurant in [mumbai](location) at an average cost of around [800](budget)
- cost of [500](budget)
- [costly](budget) restaurants
- cost of [150](budget)
- restaurant average cost of two between [550](price_lowerbound) to [950](price_upperbound)
- I want to have a [costly](budget) dinner today
- please suggest me a [cost effective](budget) place with [good](rating:4) menu
- find me a restaurant in [budget](budget:low) range
- is there a restaurant in [450](price_lowerbound) - [600](price_upperbound)
- max to max [800](price_upperbound)
- minimum [350](price_lowerbound)
- minimum [200](price_lowerbound) per person
- maximum [300](price_lowerbound) per person
- average cost of [rs800](budget:800)
- [rs1000](budget:1000)
- approx [800](budget:800)
- approx [rs100](budget:100)
- Can you suggest me some [good](rating:4) restaurants in [dehradun](location)?
- under [1000](price_upperbound)
- [150](budget) would do
- please search in [Allahabad](location)
- i am hungry. find me some good [chinese](cuisine) restaurant in [chandigarh](location)
- can you please help me search a cafe in [gurgaon](location)
- [american](cuisine) restaurants in [expensive](budget:high) restaurant
- [mid](budget)
- give me top restaurants in [new delhi](location) with [mexican](cuisine) cuisine
- [Hyderabad](location)
- [Mysore](location)
- [Nashik](location)
- can you suggest me some good restaurants in [rishikesh](location)
- [Pune](location)
- [Ahmedabad](location)
- [Allahabad](location)
- Dine out in [Ranchi](location)
- [cheapest](budget:low) [chinese](cuisine) restaurant in [Guwahati](location)
- indian coffee house in [juhu](location:mumbai)
- more than [900](price_lowerbound)
- is there any restaurant in [shimoga](location) where i can have [lunch](mealtype) in [250](price_upperbound)
- search for a [south indian](cuisine) seller
- i am so hungry. please suggest me a place to eat [chinese](cuisine)
- i am so so so much hungry that i just cant wait for something [chinese](cuisine) to eat
- how many times do i have to tell you. search in [patna](location)
- dude just order something [chinese](cuisine) for me. I cant wait
- can you suggest me some good restaurants in [rishikesh](location)
- okay. show me some in [allahabad](location)

## intent:relationship
- [i](person) want to marry [you](person)
- [you](person) are so hot
- would [you](person) go on a date with [me](person)?
- I love my family
- My [mom](person) is a supermom
- are you pregnant
- is there any place [you](person) and [i](person) can meet?

## intent:intro
- What is your name?
- Who are [you](person)?
- where do you live?
- do [you](person) have any friends?

## intent:skip
- let's skip this part
- let's call it a day
- no I dont want to continue
- you know what! i am done. i dnt want to talk to you
- i do not wish to proceed
- stop
- end this
- end the convo
- nevermind. i will search elsewhere
- fine. i dont need your help.

## intent:random_conversation
- [Dad](person) and [mom](person) are out today on shopping
- let's play [football](product)
- hey do you think [india](country) will win next [World Cup](event)?
- Today is my [birthday](event)
- [COVID](disease) is spreading fast
- who is the [Prime Minister](designation) of [India](country)?
- It's so hot today in [Egypt](location)
- Our [Principal](designation) ordered everyone to march in line
- [President](designation) Abraham Lincoln was the best USA ever got
- [COVID](disease) is a deadly disease
- Suggest me a nice [novel](product) to read
- recommend me a [horror](genre) [movie](product) today
- play some [rock](genre) [songs](product)
- i want to buy [ferrari](product)
- search for a sofa seller
- lo safar shuru ho gya
- i would like to play a game
- do you have any feature so that you can fly
- today there is a powercut in my area
- book me a ticket for 9PM show in [mumbai](location)
- Jeff bezos is the richest man in [america](location)
- what is [79USD](budget) in INR
- USD is currency of USA

## intent:geosearch
- find a restaurant nearby
- is there any restaurant near me?
- punjabi restaurant near me
- low budget restaurants near me
- is there any place nearby to wash my hands?
- I want to order food at my home

## intent:email_input
- [clementine12_dj@gmail.com](mail_id)
- SAIBI SINGHAI
- saibi singhai
- rock and roll baby
- [clementine12_dj@gmail.com](mail_id) is my mail id. send it there

## intent:affirm+email_input
- yes send it to [clementine12_dj@gmail.com](mail_id)
- sure. my email id is [clementine12_dj@gmail.com](mail_id)
- of course. [clementine12_dj@gmail.com](mail_id) is my mail id. send it there
- yep. send results to [clementine12_dj@gmail.com](mail_id)
- looks good. my email id is [clementine12_dj@gmail.com](mail_id) 
- [upgradtest123@gmail.com](mail_id)
- SAIBI SINGHAI
- saibi singhai
- of course. rock and roll baby

## synonym:4
- good
- okayish

## synonym:5
- 5 star

## synonym:Puducherry
- Pondicherry

## synonym:Ahmedabad
- Amdavad
- Ahmadabad
- Ahemdabad

## synonym:Allahabad
- Allahabad
- Alahabad
- Ilahabad
- Illahabad
- Prayag
- Prayaag
- Prayagraj

## synonym:New Delhi
- Delhi
- Dilli

## synonym:Gurgaon
- Gurugram
- Gudgaon
- Gurgao

## synonym:Guwahati
- Guhati

## synonym:Mangalore
- Kodiyal
- Mangaluru
- Mangalapuram

## synonym:Pune
- Poona
- Puna

## synonym:Mumbai
- Mmbai
- Bombay
- Bombai
- Bambai
- Navi Mumbai
- Old Mumbai
- Andheri
- Wadala
- Juhu
- Kolaba
- Bandra

## synonym:Mysore
- Mysuru
- Mysor
- Msore
- Mahishuru
- Mahishapura

## synonym:Nashik
- Nasik
- Naasik
- Naashik
- Gulshanabad

## synonym:Noida
- NCR
- Greater Noida

## synonym:Patna
- Pataliputra
- Kusumapura
- Pushpapuram

## synonym:Ranchi
- Archi
- Rachi
- Ranchi City

## synonym: Hyderabad
- secunderabad
- hydrabad

## synonym:bangalore
- Bengaluru

## synonym:high
- expensive
- high budget
- luxurious
- costly
- lavish
- elite
- high class
- more than 700
- more than 700
- morethan 700
- greater than 700

## synonym:low
- budget
- low budget
- cost effective
- budget friendly
- cheap

## synonym:mid
- moderate
- medium budget
- medium
- average

## synonym:mexican
- burrito bowl
- mexican burrito bowl
- churros
- mexican Chorizo
- Mole Sauce
- tamales
- Tacos al Pastor

## synonym:american
- burger
- chicken
- french fries
- sandwich
- double-decker sandwich
- california sandwich 
- crabcakes
- potato chips
- fortune cookies
- popcorn
- pie
- apple pie
- fruit pie

## synonym:italian
- pizza
- farmhouse pizza
- peppy paneer pizza
- overloaded pizza
- pasta
- alfredo pasta
- Arrabaita pasta
- red sauce pasta
- white sauce pasta
- tacos
- Risotto ai gamberoni

## synonym:chinese
- noodles
- hakka noodles
- manchurian
- gobhi manchurian
- spring rolls
- fried rice
- maggie
- schezwan rice
- schezwan fried rice


## synonym:north indian
- paneer do pyaza
- samosa
- kachori
- naan
- khaman
- khandwi
- paneer tikka
- dal makhni
- pav bhaji
- kadhai paneer
- shahi paneer
- chhole bhature
- chhole kulche
- dal tadka
- dal fry
- chana masala
- chaat
- aloo chaat
- fafda
- gajar ka halwa

## synonym:south indian
- Idli Sambhar
- south indian flavours
- medu wada
- idli sambhar
- wada sambhar
- uttapam
- appam
- rasam chawal
- curd rice
- dahi chawal
- biryani
- mutton biryani
- masala dosa
- rawa dosa
- paper dosa


## regex:mail_id
- [a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+

## lookup:location
- New Delhi
- Gurgaon
- Noida
- Faridabad
- Allahabad
- Bhubaneshwar
- Mangalore
- Mumbai
- Ranchi
- Patna
- Mysore
- Aurangabad
- Amritsar
- Puducherry
- Varanasi
- Nagpur
- Vadodara
- Dehradun
- Vizag
- Agra
- Ludhiana
- Kanpur
- Lucknow
- Surat
- Kochi
- Indore
- Ahmedabad
- Coimbatore
- Chennai
- Guwahati
- Jaipur
- Hyderabad
- Bangalore
- Nashik
- Pune
- Kolkata
- Bhopal
- Goa
- Chandigarh
- Ghaziabad
- Ooty
- Gangtok
- Shimla
