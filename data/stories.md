## Story_1
* greet
  - utter_greet
* restaurant_search
  - utter_ask_location
* restaurant_search{"location": "delhi"}
  - slot{"location": "delhi"}
  - action_check_location
  - slot{"location": "delhi"}
  - slot{"valid_location": "found"}
  - utter_ask_cuisine
* restaurant_search{"cuisine": "chinese"}
  - slot{"cuisine": "chinese"}
  - utter_ask_budget_pref
* affirm
  - utter_ask_budget
* restaurant_search{"budget": "more than 700"}
  - slot{"budget": "more than 700"}
  - action_search_restaurants
  - slot{"restaurant_found": "True"}
  - utter_email_pref
* affirm
  - utter_email_id
* email_input{"mail_id":"xyz@gmail.com"}
  - slot{"mail_id":"xyz@gmail.com"}
  - action_check_email
  - slot{"invalid_mail": "False"}
  - action_send_mail
  - action_restart

## Story_2
* greet
  - utter_greet
* restaurant_search
  - utter_ask_location
* restaurant_search{"location": "abcd"}
  - slot{"location": "abcd"}
  - action_check_location
  - slot{"location": "null"}
  - slot{"valid_location": "not_found"}
  - utter_notfound
* restaurant_search{"location": "delhi"}
  - slot{"location": "delhi"}
  - utter_ask_cuisine
* restaurant_search{"cuisine": "chinese"}
  - slot{"cuisine": "chinese"}
  - utter_ask_budget_pref
* affirm
  - utter_ask_budget
* restaurant_search{"budget": "more than 700"}
  - slot{"budget": "more than 700"}
  - action_search_restaurants
  - slot{"restaurant_found": "True"}
  - utter_email_pref
* affirm
  - utter_email_id
* email_input{"mail_id":"xyz@gmail.com"}
  - slot{"mail_id":"xyz@gmail.com"}
  - action_check_email
  - slot{"invalid_mail": "False"}
  - action_send_mail
  - utter_goodbye
  - action_restart

## Story_3
* greet
  - utter_greet
* restaurant_search
  - utter_ask_location
* restaurant_search{"location": "majholi"}
  - slot{"location": "majholi"}
  - action_check_location
  - slot{"location": "null"}
  - slot{"valid_location": "not_operable"}
  - utter_not_operable
* restaurant_search{"location": "delhi"}
  - slot{"location": "delhi"}
  - utter_ask_cuisine
* restaurant_search{"cuisine": "chinese"}
  - slot{"cuisine": "chinese"}
  - utter_ask_budget_pref
* affirm
  - utter_ask_budget
* restaurant_search{"budget": "more than 700"}
  - slot{"budget": "more than 700"}
  - action_search_restaurants
  - slot{"restaurant_found": "True"}
  - utter_email_pref
* affirm
  - utter_email_id
* email_input{"mail_id":"xyz@gmail.com"}
  - slot{"mail_id":"xyz@gmail.com"}
  - action_check_email
  - slot{"invalid_mail": "False"}
  - action_send_mail
  - utter_goodbye
  - action_restart

## Story_4
* greet
    - utter_greet
* restaurant_search{"location": "nashik"}
    - slot{"location": "nashik"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "nashik"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "Italian"}
    - slot{"cuisine": "Italian"}
    - utter_ask_budget_pref
* affirm
  - utter_ask_budget
* restaurant_search{"budget": "high"}
    - slot{"budget": "high"}
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* affirm
    - utter_email_id
* email_input
    - utter_invalid_email
* email_input
    - utter_invalid_email
    - utter_max_invalid
    - utter_goodbye
    - action_restart

## Story_5
* greet
    - utter_greet
* restaurant_search
    - utter_ask_location
* restaurant_search{"location": "New Delhi"}
    - slot{"location": "New Delhi"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "New Delhi"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "Italian"}
    - slot{"cuisine": "Italian"}
    - utter_ask_budget_pref
* affirm
  - utter_ask_budget
* restaurant_search{"budget": "high"}
    - slot{"budget": "high"}
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* deny
    - utter_goodbye
    - action_restart

## Story_6
* greet
    - utter_greet
* restaurant_search{"cuisine": "chinese"}
    - slot{"cuisine": "chinese"}
    - utter_ask_location
* restaurant_search{"location": "coimbatore"}
    - slot{"location": "coimbatore"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "coimbatore"}
    - utter_ask_budget_pref
* affirm
    - utter_ask_budget
* restaurant_search{"budget": "mid"}
    - slot{"budget": "mid"}
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* deny
    - utter_goodbye
    - action_restart

## Story_7
* restaurant_search
    - utter_ask_location
* restaurant_search{"location": "agra"}
    - slot{"location": "agra"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "agra"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "Italian"}
    - slot{"cuisine": "Italian"}
    - utter_ask_budget_pref
* deny
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* affirm
    - utter_email_id
* email_input{"mail_id": "upgradtest123@gmail.com"}
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - action_send_mail
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - utter_goodbye
    - action_restart

# Story_8
* greet
    - utter_greet
* restaurant_search{"budget": "low"}
    - slot{"budget": "low"}
    - utter_ask_location
* restaurant_search{"location": "Mumbai"}
    - slot{"location": "Mumbai"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "Mumbai"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "American"}
    - slot{"cuisine": "American"}
    - action_search_restaurants
    - slot{"restaurant_found": false}
    - utter_goodbye
    - action_restart

## Story_9
* greet
    - utter_greet
* geosearch
    - utter_geofence
* greet
    - utter_greet
* restaurant_search{"rating": "4", "cuisine": "north indian", "location": "mumbai"}
    - slot{"cuisine": "north indian"}
    - slot{"location": "mumbai"}
    - slot{"rating": "4"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "mumbai"}
    - utter_ask_budget_pref
* deny
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* deny
    - utter_goodbye
    - action_restart


## Story_10
* greet
    - utter_greet
* restaurant_search{"cuisine": "american", "price_lowerbound": "300"}
    - slot{"cuisine": "american"}
    - slot{"price_lowerbound": "300"}
    - utter_ask_location
* restaurant_search{"location": "kanpur"}
    - slot{"location": "kanpur"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "kanpur"}
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* deny
    - utter_goodbye
    - action_restart


## Story_11
* restaurant_search
    - utter_ask_location
* restaurant_search{"location": "aligarh"}
    - slot{"location": "aligarh"}
    - action_check_location
    - slot{"valid_location": "not_operable"}
    - slot{"location": null}
    - utter_not_operable
* restaurant_search{"location": "bhavnagar"}
    - slot{"location": "bhavnagar"}
    - action_check_location
    - slot{"valid_location": "not_operable"}
    - slot{"location": null}
    - utter_not_operable
    - utter_max_invalid
    - utter_goodbye
    - action_restart
* greet
    - utter_greet

    
## Story_12
* restaurant_search{"location": "gangtok", "cuisine": "chinese"}
    - slot{"cuisine": "chinese"}
    - slot{"location": "gangtok"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "gangtok"}
    - utter_ask_budget_pref
* affirm
    - utter_ask_budget
* restaurant_search{"price_lowerbound": "900"}
    - slot{"price_lowerbound": "900"}
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* deny
    - utter_goodbye
    - action_restart


## Story_13
* restaurant_search{"cuisine": "south indian", "price_upperbound": "400"}
    - slot{"cuisine": "south indian"}
    - slot{"price_upperbound": "400"}
    - utter_ask_location
* restaurant_search{"location": "jaipur"}
    - slot{"location": "jaipur"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "jaipur"}
    - action_search_restaurants
    - slot{"restaurant_found": false}
    - utter_goodbye
    - action_restart

## Story_14
* restaurant_search{"location": "mumbai"}
    - slot{"location": "mumbai"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "mumbai"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "chinese"}
    - slot{"cuisine": "chinese"}
    - utter_ask_budget_pref
* deny
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* affirm
    - utter_email_id
* email_input{"mail_id":"xyz@gmail.com"}
    - slot{"mail_id":"xyz@gmail.com"}
    - action_check_email
    - slot{"invalid_mail": "True"}
    - utter_invalid_email
    - utter_rephrase
* email_input{"mail_id":"xyz@gmail.com"}
    - slot{"mail_id":"xyz@gmail.com"}
    - action_check_email
    - slot{"invalid_mail": "False"}
    - slot{"invalid_mail": "False"}
    - action_send_mail
    - utter_goodbye
    - action_restart

## Story_15
* greet
    - utter_greet
* restaurant_search{"location": "nashik"}
    - slot{"location": "nashik"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "nashik"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "Italian"}
    - slot{"cuisine": "Italian"}
    - utter_ask_budget
* restaurant_search{"budget": "high"}
    - slot{"budget": "high"}
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* affirm
    - utter_email_id
* email_input{"mail_id":"xyz@gmail.com"}
    - slot{"mail_id":"xyz@gmail.com"}
    - action_check_email
    - slot{"invalid_mail": "True"}
    - utter_invalid_email
    - utter_rephrase
* email_input{"mail_id":"xyz@gmail.com"}
    - slot{"mail_id":"xyz@gmail.com"}
    - action_check_email
    - slot{"invalid_mail": "True"}
    - utter_max_invalid
    - utter_goodbye
    - action_restart

## Story_16
* restaurant_search{"cuisine": "indian", "location": "mumbai"}
    - slot{"cuisine": "indian"}
    - slot{"location": "mumbai"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "mumbai"}
    - utter_ask_budget_pref
* affirm
    - utter_ask_budget
* restaurant_search{"budget": "mud"}
    - slot{"budget": "mud"}
    - action_search_restaurants
    - slot{"restaurant_found": false}
    - utter_goodbye
    - action_restart

##  Story_17
* restaurant_search{"cuisine": "south indian"}
    - slot{"cuisine": "south indian"}
    - utter_ask_location
* restaurant_search{"location": "ooty"}
    - slot{"location": "ooty"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "ooty"}
    - utter_ask_budget_pref
* affirm
    - utter_ask_budget
* restaurant_search{"price_upperbound": "600"}
    - slot{"price_upperbound": "600"}
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* affirm
    - utter_email_id
* email_input{"mail_id": "upgradtest123@gmail.com"}
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - action_check_email
    - slot{"invalid_mail": false}
    - action_send_mail
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - utter_goodbye
    - action_restart

## Story_18
* relationship{"person": "me"}
    - utter_relationship
    - action_restart


## Story_19
* restaurant_search{"location": "ooty", "cuisine": "mexican"}
    - slot{"cuisine": "mexican"}
    - slot{"location": "ooty"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "ooty"}
    - utter_ask_budget_pref
* deny
    - action_search_restaurants
    - slot{"restaurant_found": false}
    - utter_goodbye
    - action_restart

## Story_20
* intro
    - utter_intro
* geosearch
    - utter_geofence
    - action_restart

## Story_21
* restaurant_search{"location": "Mumbai"}
    - slot{"location": "Mumbai"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "Mumbai"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "South Indian"}
    - slot{"cuisine": "South Indian"}
    - utter_ask_budget_pref
* restaurant_search{"price_lowerbound": "900"}
    - slot{"price_lowerbound": "900"}
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* affirm+email_input{"mail_id": "upgradtest123@gmail.com"}
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - action_check_email
    - slot{"invalid_mail": false}
    - action_send_mail
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - utter_goodbye
    - action_restart

## Story_22
* restaurant_search{"location": "shimoga", "mealtype": "lunch", "price_upperbound": "250"}
    - slot{"location": "shimoga"}
    - slot{"price_upperbound": "250"}
    - slot{"mealtype": "lunch"}
    - action_check_location
    - slot{"valid_location": "not_found"}
    - slot{"location": null}
    - utter_notfound
    - utter_rephrase
* restaurant_search{"location": "shimoga"}
    - slot{"location": "shimoga"}
    - action_check_location
    - slot{"valid_location": "not_found"}
    - slot{"location": null}
    - utter_notfound
    - utter_max_invalid
    - utter_goodbye
    - action_restart

## Story_23
* restaurant_search{"cuisine": "south indian"}
    - slot{"cuisine": "south indian"}
    - utter_ask_location
* restaurant_search{"location": "bhilai"}
    - slot{"location": "bhilai"}
    - action_check_location
    - slot{"valid_location": "not_operable"}
    - slot{"location": null}
    - utter_not_operable
* restaurant_search{"location": "vizag"}
    - slot{"location": "vizag"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "vizag"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "Mexican"}
    - slot{"cuisine": "Mexican"}
    - utter_ask_budget_pref
* restaurant_search
    - utter_default
    - utter_rephrase
* restaurant_search
    - utter_default
    - utter_max_invalid
    - utter_goodbye
    - action_restart

## Story_24
* random_conversation
    - utter_random_reply
    - action_restart

## Story_25
* random_conversation{"product": "ferrari"}
    - utter_random_reply
    - action_restart

## Story_26
* greet
    - utter_greet
* random_conversation
    - utter_random_reply

## Story_27
* greet
    - utter_greet
* random_conversation
    - utter_random_reply
    - action_restart

## Story_28
* random_conversation{"location": "mumbai"}
    - slot{"location": "mumbai"}
    - utter_random_reply
    - action_restart

## Story_29
* greet
    - utter_greet
* random_conversation{"location": "america"}
    - slot{"location": "america"}
    - utter_random_reply
    - action_restart

## Story_30
* random_conversation{"budget": "79USD"}
    - slot{"budget": "79USD"}
    - utter_random_reply
    - action_restart

## Story_31
* greet
    - utter_greet
* restaurant_search{"location": "ludhiana"}
    - slot{"location": "ludhiana"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "ludhiana"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "Italian"}
    - slot{"cuisine": "Italian"}
    - utter_ask_budget_pref
* affirm
    - utter_ask_budget
* restaurant_search{"budget": "low"}
    - slot{"budget": "low"}
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* affirm
    - utter_email_id
* email_input
    - utter_invalid_email
    - utter_rephrase
* email_input{"mail_id": "upgradtest123@gmail.com"}
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - action_check_email
    - slot{"invalid_mail": false}
    - action_send_mail
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - utter_goodbye
    - action_restart

## Story_32
* greet
    - utter_greet
* restaurant_search{"cuisine": "chinese"}
    - slot{"cuisine": "chinese"}
    - utter_ask_location
* restaurant_search{"location": "patna"}
    - slot{"location": "patna"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "patna"}
    - utter_ask_budget_pref
* deny
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* email_input
    - utter_invalid_email
    - utter_rephrase
* email_input
    - utter_invalid_email
    - utter_max_invalid
    - utter_goodbye
    - action_restart

## Story_33
* greet
    - utter_greet
* restaurant_search{"location": "rishikesh"}
    - slot{"location": "rishikesh"}
    - action_check_location
    - slot{"valid_location": "not_operable"}
    - slot{"location": null}
    - utter_not_operable
* restaurant_search{"location": "allahabad"}
    - slot{"location": "allahabad"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "allahabad"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "Chinese"}
    - slot{"cuisine": "Chinese"}
    - utter_ask_budget_pref
* affirm
    - utter_ask_budget
* restaurant_search
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* affirm+email_input{"mail_id": "upgradtest123@gmail.com"}
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - action_check_email
    - slot{"invalid_mail": false}
    - action_send_mail
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - utter_goodbye
    - action_restart

## Story_34
* greet
    - utter_greet
* restaurant_search{"location": "kolkata"}
    - slot{"location": "kolkata"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "kolkata"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "American"}
    - slot{"cuisine": "American"}
    - utter_ask_budget_pref
* affirm
    - utter_ask_budget
* restaurant_search
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* affirm
    - utter_email_id
* email_input{"mail_id": "upgradtest123@gmail.com"}
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - action_check_email
    - slot{"invalid_mail": false}
    - action_send_mail
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - utter_goodbye
    - action_restart

## Story_35
* greet
    - utter_greet
* restaurant_search{"cuisine": "chinese", "location": "chandigarh"}
    - slot{"cuisine": "chinese"}
    - slot{"location": "chandigarh"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "chandigarh"}
    - utter_ask_budget_pref
* affirm
    - utter_ask_budget
* greet
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* deny
    - utter_goodbye
    - action_restart

## Story_36
* greet
    - utter_greet
* restaurant_search{"cuisine": "mexican"}
    - slot{"cuisine": "mexican"}
    - utter_ask_location
* restaurant_search{"location": "bhopal"}
    - slot{"location": "bhopal"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "bhopal"}
    - utter_ask_budget_pref
* deny
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* affirm
    - utter_email_id
* affirm+email_input{"mail_id": "upgradtest123@gmail.com"}
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - action_check_email
    - slot{"invalid_mail": false}
    - action_send_mail
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - utter_goodbye
    - action_restart

## Story_37
* thanks
    - utter_default
    - utter_max_invalid
    - utter_goodbye
    - action_restart

## Story_38
* greet
    - utter_greet
* restaurant_search{"location": "patna"}
    - slot{"location": "patna"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "patna"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "South Indian"}
    - slot{"cuisine": "South Indian"}
    - utter_ask_budget_pref
* deny
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* deny
    - utter_goodbye
    - action_restart

## Story_39
* greet
    - utter_greet
* restaurant_search{"cuisine": "mexican"}
    - slot{"cuisine": "mexican"}
    - utter_ask_location
* restaurant_search{"location": "bhopal"}
    - slot{"location": "bhopal"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "bhopal"}
    - utter_ask_budget_pref
* deny
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* affirm
    - utter_email_id
* affirm+email_input{"mail_id": "upgradtest123@gmail.com"}
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - action_check_email
    - slot{"invalid_mail": false}
    - action_send_mail
    - slot{"mail_id": "upgradtest123@gmail.com"}
    - utter_goodbye
    - action_restart

## Story_40
* thanks
    - utter_default
* thanks
    - utter_default
    - utter_rephrase
* goodbye
    - utter_goodbye
    - action_restart

## Story_41
* random_conversation{"beverage": "coffee"}
    - utter_random_reply
    - action_restart

## Story_42
* relationship
    - utter_relationship
    - action_restart

## Story_43
* greet
    - utter_greet
* greet
    - utter_greet
* restaurant_search{"cuisine": "chinese"}
    - slot{"cuisine": "chinese"}
    - utter_ask_location
* restaurant_search{"location": "indore"}
    - slot{"location": "indore"}
    - action_check_location
    - slot{"valid_location": "found"}
    - slot{"location": "indore"}
    - utter_ask_budget_pref
* affirm
    - utter_ask_budget
* restaurant_search
    - action_search_restaurants
    - slot{"restaurant_found": true}
    - utter_email_pref
* deny
    - utter_goodbye
    - action_restart

## Story_44
* restaurant_search{"cuisine": "mexican"}
    - slot{"cuisine": "mexican"}
    - utter_ask_location
* geosearch
    - utter_geofence
    - action_restart

## Story_45
* thanks
    - utter_default
    - action_restart

## Story_46
* restaurant_search{"location": "mandla"}
    - slot{"location": "mandla"}
    - action_check_location
    - slot{"valid_location": "not_found"}
    - slot{"location": null}
    - utter_notfound
    - utter_rephrase
* restaurant_search{"location": "mandla"}
    - slot{"location": "mandla"}
    - action_check_location
    - slot{"valid_location": "not_found"}
    - slot{"location": null}
    - utter_notfound
    - utter_max_invalid
    - utter_goodbye
    - action_restart

## Story_47
* restaurant_search{"cuisine": "chinese"}
    - slot{"cuisine": "chinese"}
    - utter_ask_location
* deny
    - utter_default
    - utter_rephrase
* deny
    - utter_max_invalid
    - utter_goodbye
    - action_restart

## Story_48
* restaurant_search{"cuisine": "chinese", "budget": "expensive"}
    - slot{"cuisine": "chinese"}
    - slot{"budget": "expensive"}
    - utter_ask_location
* random_conversation
    - utter_random_reply
    - action_restart

## Story_49
* restaurant_search{"cuisine": "american", "location": "bhilai"}
    - slot{"cuisine": "american"}
    - slot{"location": "bhilai"}
    - action_check_location
    - slot{"valid_location": "not_operable"}
    - slot{"location": null}
    - utter_not_operable
    - utter_rephrase
* restaurant_search{"location": "amravati"}
    - slot{"location": "amravati"}
    - action_check_location
    - slot{"valid_location": "not_operable"}
    - slot{"location": null}
    - utter_not_operable
    - utter_max_invalid
    - utter_goodbye
    - action_restart

## Story_50
* restaurant_search{"cuisine": "american"}
    - slot{"cuisine": "american"}
    - utter_ask_location
* restaurant_search{"location": "bhilai"}
    - slot{"location": "bhilai"}
    - action_check_location
    - slot{"valid_location": "not_operable"}
    - slot{"location": null}
    - utter_not_operable
    - utter_rephrase
* skip
    - utter_goodbye
    - action_restart
